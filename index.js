// const logUpdate = require('log-update')
const chalk = require('chalk')
const fs = require('fs')
const path = require('path')
const readline = require('readline')
const read = f => fs.readFileSync(path.resolve(__dirname, f), 'utf-8')
String.prototype.vars = function (vars) {
  vars = typeof vars == 'object' ? vars : arguments
  return Object
    .keys(vars)
    .reduce((m, v) => {
      let r = new RegExp('(' + v + '[₺|$|₸|₼])+'), e
      while ((e = r.exec(m)) != null) m = m.replace(e[0], vars[v])
      return m
    }, this)
}

let frame = read('./frameCompiled')
let frame2 = read('./frameCompiled')
let chars = require('tamgalar')
let icon = {
  undefined: '.',
  null: '.',
  false: chalk.red('✕'),
  true: chalk.blue('✔')
}
let tests1
let attachResolver = (prom, Case) =>
  promisify(prom)
    .then(r => Results[Case] = r != false && true)
    .catch(r => Results[Case] = false)

let promisify = f => f instanceof Promise
  ? f
  : ((f = f()) instanceof Promise
    ? f
    : new Promise((res, rej) =>
      f ? res(true) : rej(false)))

let t = 0, options
let keys,
  Frame = {
    results: {},
    t: 0,
    complete: false,
    success: false,
    interval: setInterval(() => Frame.update(), 100),
    get done() {
      let done = Object.keys(this.results).length >= keys.length

      if (done) {
        clearInterval(Frame.interval)
        setTimeout(i => {
          readline.cursorTo(process.stdout, 0, 19 + keys.length)
          process.stderr.write('\x1B[?25h')
          this.update()
          readline.cursorTo(process.stdout, 0, 22 + keys.length)

          setTimeout(() => {
            console.clear()
            console.log(frame2.vars({
              results:
                Object.keys(tests1)
                  .map((i, j) => '█   ' + icon[Results[i]] + ' : ' + i)
                  .join('\n'),
              end: '   ' + this.status
            }))

            process.exit(Number(!this.result), 1000)
          }, 15)
        })
      }
      return done
    },
    get result() {
      return Object.values(this.results)
        .reduce((c, r) => c * Number(r), 1)
    },
    get status() {
      if (!this.done) {
        return 'Kod sınanıyor     ' + chars[t++ % chars.length]
      } else {
        return chalk[this.result ? 'blue' : 'red']('Denev ' + (this.result ? 'başarılı' : 'başarısız'))
      }
    },
    update() {
      readline.cursorTo(process.stdout, 3, 19 + keys.length)
      process.stdout.write(this.status)
    }
  },
  Results = new Proxy({}, {
    get(o, k) {
      return Frame.results[k]
    },
    set(o, k, v) {
      Frame.results[k] = v
      if (options.frame) {
        readline.cursorTo(process.stdout, 4, 16 + keys.indexOf(k))

        process.stdout.write(icon[v])
        process.stderr.write('\x1B[?25l')
      }
      // Frame.update()
    }
  })

module.exports = (Test, opts = {}) => {
  options = opts = Object.assign({
    frame: true
  }, opts)
  keys = Object.keys(Test)
  if (opts.frame) {
    frame = frame.vars({
      results: keys
        .map((i, j) => '█   ? : ' + i)
        .join('\n'),
      end: ''
    })
    console.clear()
    console.log(frame)
  } else {
    clearInterval(Frame.interval)
  }
  tests1 = Test
  return Promise
    .all(keys.map(Case =>
      typeof Test[Case] == 'function' || Test[Case] instanceof Promise
        ? attachResolver(Test[Case], Case)
        : new Promise((res, rej) =>
          Test[Case].interval = setInterval(() => {
            if (!Test[Case].after.filter(i => Object.keys(Frame.results).indexOf(i) == -1).length) {
              clearInterval(Test[Case].interval)
              attachResolver(Test[Case].run, Case)
                .then(res)
                .catch(rej)
            }
          }, 100)
        )
    ))
}