# Dene

Dene is a unit testing framework to test library methods etc.

<img src="https://media.giphy.com/media/cdI9qrqmXPTtLKu5bQ/giphy.gif" width="300px">

## Installation

```bash
npm i dene --save-dev
```

## Tutorial

Write test cases


```javascript
// dene.js

let Dene=require('dene')

let Tests={
  simpleTest: () => return true,
  asyncTest:  () => 
    new Promise((pass, fail) =>
      setTimeout(pass ,1500) // Succeed after 1.5s
}

Dene(Tests)
```

Add your test script to package.json

```json
  "scripts":{
    "test":"node dene.js"
  }

```

Run tests at terminal

```bash
npm test
```

![](https://nodei.co/npm/dene.png)

## License

MIT Copyright [Otag](https://otagjs.org)
