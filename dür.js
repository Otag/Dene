const fs = require('fs')
const chalk = require('chalk')
const read = (f) => fs.readFileSync(f, 'utf-8')
const readP = f =>
  new Promise((res, rej) =>
    res(read(f)))
const writeTo = f =>
  d => fs.writeFileSync(f, d)

readP('./frame')
  .then(f => f
    .vars({
      title: chalk.blue('Dene | Birim Sınayıcı'),
      arrow: chalk.blue(read('./arrow'))
    }))
  .then(writeTo('frameCompiled'))