
module.exports = {
  komutOlarak: a => true,
  true: () => true,
  prom: () => Promise.resolve(true),
  asyncTest: () =>
    new Promise((res, rej) =>
      setTimeout(res, 3000))
}